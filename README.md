# Intro to Data Binding #

By this point in the class, we have been doing all of our coding in the code-behind file (SomeNeatoPage.xaml.cs). We have made
extensive use of event handlers in order to make our apps interactive. This works just fine, but it turns out that there is a
better way to accomplish the same sort of thing with less code.

There's an additional benefit:  The more code we can keep out of our code-behind, the more maintainable our code becomes. By separating
our view from our business logic, we create a "loose coupling" between the two. This makes it much easier to change either one without
having to change or impact the other, and it makes for much more testable code.

Data binding is one of the absolute coolest things in software development... You'll love it!


### Branches in This Repo ###

* __master__:  This is the branch with data binding implemented. Zero logic in the code-behind file.
* __usingEventHandlers__:  This branch shows how we would do the same exact thing with event handlers and the code-behind.

### Data Binding Resources
* Check out the ["Creating Mobile Apps with Xamarin.Forms"](https://developer.xamarin.com/guides/xamarin-forms/creating-mobile-apps-xamarin-forms/) book, chapter 16.
* Xamarin has some [great documentation online](https://developer.xamarin.com/guides/xamarin-forms/xaml/xaml-basics/data_binding_basics/ "Basics of Data Binding").

---
### usingEventHandlers Code-Behind:

![Logic in code behind](usingEventHandlers.png)

---
### master Code-Behind

![Empty code behind](dataBindingMaster.png)