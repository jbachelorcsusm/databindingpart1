﻿using Xamarin.Forms;
using System.Diagnostics;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DataBinding
{
    public partial class App : Application
    {
        public App()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(App)}:  ctor");
            InitializeComponent();

            MainPage = new DataBindingPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
        }
    }
}
